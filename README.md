# UI Developer Take-home Project

The purpose of this project is to assess your approach to UI development and to help us understand your strengths and weaknesses

## Task

- Build a dashboard using the API endpoint accessible at http://localhost:3000

## How do I run the API?

1. Make sure you have NodeJS installed (https://nodejs.org/en/)
2. Run `npm install`
3. Run `node server.js`
4. You should see "Ship data accessible at http://localhost:3000"

## Bonus

- Mobile Responsive
- Creativity
