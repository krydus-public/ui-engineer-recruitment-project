const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  const dashboardData = {
    shipName: "GIANT_MK1",
    status: "DOCKED",
    velocity: {
      value: 0,
      unit: "km/s",
    },
    altitude: {
      value: 0.0,
      unit: "km",
    },
    cabin: {
      temperature: { value: 0, unit: "c" },
      pressure: { value: 0, unit: "psia" },
      oxygen: {value: 1.00, unit: 'mmHg'},
      co2: {value: 0.02, unit: 'mmHg'}
    },
    connections: {
        airlock: false,
        wing: true,
        rig: false
    }
  };
  res.json(dashboardData);
});

app.listen(port, () => {
  console.log(`Ship data accessible at http://localhost:${port}`);
});
